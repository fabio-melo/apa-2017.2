# Algorítimos de Ordenação - MergeSort e Quicksort em Python 3

* Modo de Uso:
na linha de comando (Requer Python 3.6)
```
selectionsort.py [Arquivo de Entrada] [Arquivo de Saida]
insertionsort.py [Arquivo de Entrada] [Arquivo de Saida]
```
```
selectionsort.py teste1.txt saidateste1.txt
insertionsort.py teste2.txt saidateste2.txt
```
